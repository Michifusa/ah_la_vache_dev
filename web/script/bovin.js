/*========================
	Ajout Vache / Taureau
=====================*/

$('#form_add_vache').submit(function (e) {
	e.preventDefault();
	
	if ($('#bovinbundle_bovin_bovinNumero').val().length == 4 ) {
		bovinNumero = true;
	}else{
		$('#bovinbundle_bovin_bovinNumero').addClass('form-error');
		$('.form-control-error-msg.bovinNumero').css('display','block');
		bovinNumero = false;
	}

	if ($('#bovinbundle_bovin_bovinNaissance').val().length > 1) {
		bovinNaissance = true;
	}else{
		$('#bovinbundle_bovin_bovinNaissance').addClass('form-error');
		$('.form-control-error-msg.bovinNaissance').css('display','block');
		bovinNaissance = false;
	}

	if(bovinNumero && bovinNaissance){
		$.ajax({
	      type: "POST",
	      url: Routing.generate('vache_ajout',{'groupe':$('#groupeId').val()}),
	      data: $('#form_add_vache').serialize(),
	      success: function(data) {
	      	if(data != 'erreur_numero'){
	      		loadTableVache()
	      		$('#modalAddVache').modal('hide')
	      		$.confirm({
		          icon: 'fa fa-check',
		          type: 'green',
		          title: 'Félicitation !',
		          content: 'La Vache a bien été ajouté, souhaitez-vous aller sur sa page ?',
		          backgroundDismiss: true,
		          buttons: {
		                supprimer: {
		                    text: 'Oui',
		                    btnClass: 'btn-green',
		                    action: function(){
		                    	window.location.href= Routing.generate('fiche_vache',{'groupe':$('#groupeId').val(),'numero':data});
		                    }
		                },
		                fermer: {
		                    text: 'Non',
		                    btnClass: 'btn-secondary',
		                    action: function(){
		                    }
		                },
	            	}
		     	});
	      	}else if(data == 'erreur_numero'){
	      		$('#bovinbundle_bovin_bovinNumero').addClass('form-error');
				$('.form-control-error-msg.bovinNumero2').css('display','block');
	      	}
	      }
	    });
	}
})

$('#form_add_taureau').submit(function (e) {
	e.preventDefault();
	
	if ($('#bovinbundle_bovin_bovinNumero').val().length == 4 ) {
		bovinNumero = true;
	}else{
		$('#bovinbundle_bovin_bovinNumero').addClass('form-error');
		$('.form-control-error-msg.bovinNumero').css('display','block');
		bovinNumero = false;
	}

	if ($('#bovinbundle_bovin_bovinNaissance').val().length > 1) {
		bovinNaissance = true;
	}else{
		$('#bovinbundle_bovin_bovinNaissance').addClass('form-error');
		$('.form-control-error-msg.bovinNaissance').css('display','block');
		bovinNaissance = false;
	}

	if(bovinNumero && bovinNaissance){
		$.ajax({
	      type: "POST",
	      url: Routing.generate('taureau_ajout',{'groupe':$('#groupeId').val()}),
	      data: $('#form_add_taureau').serialize(),
	      success: function(data) {
	      	if(data != 'erreur_numero'){
	      		loadTableTaureau()
	      		$('#modalAddTaureau').modal('hide')
	      		$.confirm({
		          icon: 'fa fa-check',
		          type: 'green',
		          title: 'Félicitation !',
		          content: 'Le Taureau a bien été ajouté, souhaitez-vous aller sur sa page ?',
		          backgroundDismiss: true,
		          buttons: {
		                supprimer: {
		                    text: 'Oui',
		                    btnClass: 'btn-green',
		                    action: function(){
		                    	window.location.href= Routing.generate('fiche_taureau',{'groupe':$('#groupeId').val(),'numero':data});
		                    }
		                },
		                fermer: {
		                    text: 'Non',
		                    btnClass: 'btn-secondary',
		                    action: function(){
		                    }
		                },
	            	}
		     	});
	      	}else if(data == 'erreur_numero'){
	      		$('#bovinbundle_bovin_bovinNumero').addClass('form-error');
				$('.form-control-error-msg.bovinNumero2').css('display','block');
	      	}
	      }
	    });
	}
})

$('#bovinbundle_bovin_bovinNumero').on('focusout',function () {
	$('#bovinbundle_bovin_bovinNumero').removeClass('form-error');
	$('.form-control-error-msg.bovinNumero').css('display','none');
	$('.form-control-error-msg.bovinNumero2').css('display','none');
})

$('#bovinbundle_bovin_bovinNaissance').on('focusout',function () {
	$('#bovinbundle_bovin_bovinNaissance').removeClass('form-error');
	$('.form-control-error-msg.bovinNaissance').css('display','none');
})

/*========================
	Fiche Vache / Taureau
=====================*/


$('#btn_modif_fiche_vache').on('click',function (){
	$('#vacheBovinNom').removeAttr('disabled').addClass('form-editable');
	$('#vacheBovinNumero').removeAttr('disabled').addClass('form-editable');
	$('#vacheBovinNaissance').removeAttr('disabled').addClass('form-editable');
	$('#selectBovinRace').css('display','');
	$('#inputBovinRace').css('display','none');
	$('#vacheBovinSortie').removeAttr('disabled').addClass('form-editable');
	$('#selectBovinStatutSortie').css('display','');
	$('#inputBovinStatutSortie').css('display','none');
	$('#selectBovinNaissanceFacilite').css('display','');
	$('#inputBovinNaissanceFacilite').css('display','none');
	$('#selectBovinClassement').css('display','');
	$('#inputBovinClassement').css('display','none');
	$('#btn_annule_fiche_vache').css('display','');
	$('#btn_save_fiche_vache').css('display','');
	$('#btn_modif_fiche_vache').css('display','none');

	$("html, body").animate({ scrollTop: 0 }, 600);
})

$('#btn_annule_fiche_vache').on('click',function (){
	location.reload();
})

$('#btn_modif_fiche_taureau').on('click',function (){
	$('#taureauBovinNom').removeAttr('disabled').addClass('form-editable');
	$('#taureauBovinNumero').removeAttr('disabled').addClass('form-editable');
	$('#taureauBovinNaissance').removeAttr('disabled').addClass('form-editable');
	$('#selectBovinRace').css('display','');
	$('#inputBovinRace').css('display','none');
	$('#taureauBovinSortie').removeAttr('disabled').addClass('form-editable');
	$('#selectBovinStatutSortie').css('display','');
	$('#inputBovinStatutSortie').css('display','none');
	$('#selectBovinNaissanceFacilite').css('display','');
	$('#inputBovinNaissanceFacilite').css('display','none');
	$('#selectBovinClassement').css('display','');
	$('#inputBovinClassement').css('display','none');
	$('#btn_annule_fiche_taureau').css('display','');
	$('#btn_save_fiche_taureau').css('display','');
	$('#btn_modif_fiche_taureau').css('display','none');

	$("html, body").animate({ scrollTop: 0 }, 600);
})

$('#btn_annule_fiche_taureau').on('click',function (){
	location.reload();
})

$('#form_modif_fiche_vache').submit(function (e) {
	e.preventDefault();

	if ($('#vacheBovinNumero').val().length == 4 ) {
		bovinNumero = true;
	}else{
		$('#vacheBovinNumero').addClass('form-error');
		$('.form-control-error-msg.bovinNumero').css('display','block');
		bovinNumero = false;
	}

	if ($('#vacheBovinNaissance').val().length > 1) {
		bovinNaissance = true;
	}else{
		$('#vacheBovinNaissance').addClass('form-error');
		$('.form-control-error-msg.bovinNaissance').css('display','block');
		bovinNaissance = false;
	}

	if(bovinNumero && bovinNaissance){
		$(this)[0].submit();
	}else{
		$("html, body").animate({ scrollTop: 0 }, 600);
	}

});

$('#form_modif_fiche_taureau').submit(function (e) {
	e.preventDefault();

	if ($('#taureauBovinNumero').val().length == 4 ) {
		bovinNumero = true;
	}else{
		$('#taureauBovinNumero').addClass('form-error');
		$('.form-control-error-msg.bovinNumero').css('display','block');
		bovinNumero = false;
	}

	if ($('#taureauBovinNaissance').val().length > 1) {
		bovinNaissance = true;
	}else{
		$('#taureauBovinNaissance').addClass('form-error');
		$('.form-control-error-msg.bovinNaissance').css('display','block');
		bovinNaissance = false;
	}

	if(bovinNumero && bovinNaissance){
		$(this)[0].submit();
	}else{
		$("html, body").animate({ scrollTop: 0 }, 600);
	}

});

$('#vacheBovinNumero').on('focusout',function () {
	$('#vacheBovinNumero').removeClass('form-error');
	$('.form-control-error-msg.bovinNumero').css('display','none');
	$('.form-control-error-msg.bovinNumero2').css('display','none');
})

$('#vacheBovinNaissance').on('focusout',function () {
	$('#vacheBovinNaissance').removeClass('form-error');
	$('.form-control-error-msg.bovinNaissance').css('display','none');
})

$('#taureauBovinNumero').on('focusout',function () {
	$('#taureauBovinNumero').removeClass('form-error');
	$('.form-control-error-msg.bovinNumero').css('display','none');
	$('.form-control-error-msg.bovinNumero2').css('display','none');
})

$('#taureauBovinNaissance').on('focusout',function () {
	$('#taureauBovinNaissance').removeClass('form-error');
	$('.form-control-error-msg.bovinNaissance').css('display','none');
})