// PDF Viewer

$("#modalPDFViewer").on("hidden.bs.modal", function () { 
  $('#modalPDFViewer .modal-body #pdfIframe').removeAttr('src')
  //$('#modalPDFViewer .modal-body').empty();
});

function pdfIframeLoad(src) {
  $('#modalPDFViewer .pdf-loading').show();
  $.ajax({
    url: src,
    type: 'POST',
    processData: false,
    contentType: false,
    success: function(data) {
        $('#modalPDFViewer .modal-body #pdfIframe').attr('src',data)
        $('#modalPDFViewer .pdf-loading').hide()
   }
  });
  $('#modalPDFViewer').modal('show')
}

function pdfIframeFullScreen(argument) {
	$('#pdfIframe').fullScreen(true);
}

// Load all component


$(document).ready(function () {
  $(".dataTable").DataTable({
      responsive: true,
      "language": {
          "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
      },
      initComplete: function(){
        $('.dataTables_wrapper select').select2({
          theme: "bootstrap",
          minimumResultsForSearch: -1
        });
      }
  })

  $('.selectpicker').select2({
    theme: "bootstrap",
    width: '100%'
  });
})

$(document).ready(function () {
  $('.datepicker').datepicker();
})