/*========================
	Registration
=====================*/

$('#fos_user_registration_form_email').on('focusout',function () {
	$('#fos_user_registration_form_email').removeClass('form-error');
	$('#error_email').css('display','none');
})

$('#fos_user_registration_form_username').on('focusout',function () {
	$('#fos_user_registration_form_username').removeClass('form-error');
	$('#error_username').css('display','none');
})

$('#fos_user_registration_form_plainPassword_first').on('focusout',function () {
	$('#fos_user_registration_form_plainPassword_second').removeClass('form-error');
	$('#fos_user_registration_form_plainPassword_first').removeClass('form-error');
	$('#error_password_second').css('display','none');
})

$('#fos_user_registration_form_plainPassword_second').on('focusout',function () {
	$('#fos_user_registration_form_plainPassword_second').removeClass('form-error');
	$('#fos_user_registration_form_plainPassword_first').removeClass('form-error');
	$('#error_password_second').css('display','none');
})

$( document ).ready(function() {
  if($.trim($("#error_email").html())!=''){
  	$('#fos_user_registration_form_email').addClass('form-error');
  }
  if($.trim($("#error_username").html())!=''){
  	$('#fos_user_registration_form_username').addClass('form-error');
  }
  if($.trim($("#error_password_second").html())!=''){
  	$('#fos_user_registration_form_plainPassword_first').addClass('form-error');
  	$('#fos_user_registration_form_plainPassword_second').addClass('form-error');
  }
});

/*========================
	Login
=====================*/

$('#username').on('focusout',function () {
	$('#username').removeClass('form-error');
	$('#password').removeClass('form-error');
	$('#error_log').css('display','none');
})

$('#password').on('focusout',function () {
	$('#username').removeClass('form-error');
	$('#password').removeClass('form-error');
	$('#error_log').css('display','none');
})

$( document ).ready(function() {
  if($.trim($("#error_log").html())!=''){
  	$('#username').addClass('form-error');
  	$('#password').addClass('form-error');
  }
});