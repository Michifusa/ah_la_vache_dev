<?php

namespace BovinBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TaureauType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('bovinNom', TextType::class, array('label' => 'Nom','required'=>false,'attr'=>array('class'   => 'form-control')))
            ->add('bovinNumero', IntegerType::class, array('label' => 'Numéro','required'=>false,'attr'=>array('class'   => 'form-control')))
            ->add('bovinNaissance', TextType::class, array('label' => 'Date de Naissance','required'=>false,'attr'=>array('class' => 'form-control datepicker' )))

            ->add('bovinRace', EntityType::class, array('class' => 'AppBundle:Race','query_builder' => 
                function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')->leftjoin('r.raceType','a')->where('a.id = 1')->orderBy('r.raceNom', 'ASC');
                }
                ,'label' =>'Race','required'=>true, 'choice_label' => 'raceNom','choice_value'=>'raceCode','attr'=>array('class'   => ' form-control selectpicker')))

            ->add('bovinClassement', EntityType::class, array('class' => 'AppBundle:Classement','query_builder' => 
                function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')->leftjoin('c.classementType','a')->leftjoin('c.classementSexe','se')->where('a.id = 1')->andwhere('se.id = 1')->orderBy('c.classementNom', 'ASC');
                }
                ,'label' =>'Classement','required'=>true, 'choice_label' => 'classementNom','attr'=>array('class'   => 'form-control selectpicker')))

            ->add('bovinNaissanceFacilite', EntityType::class, array('class' => 'AppBundle:NaissanceFacilite','query_builder' => 
                function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')->orderBy('n.naissanceFaciliteNom', 'ASC');
                }
                ,'label' =>'Facilité de vêlage','required'=>true, 'choice_label' => 'naissanceFaciliteNom','attr'=>array('class'   => 'form-control selectpicker')))

            ->add('save', SubmitType::class, array('label' =>'Enregistrer','attr' => array('class' => 'btn btn-bovin pull-right')));

            
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bovinbundle_bovin';
    }


}
