<?php
namespace BovinBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use BovinBundle\Entity\Bovin;

class BovinManager{

	private $entityManager;

    public function __construct(EntityManager $entityManager,ContainerInterface $containerInterface)
    {
    	$this->em = $entityManager;
    	$this->repositoryBovin = $this->em->getRepository('BovinBundle:Bovin');
    	$this->repositorySexe = $this->em->getRepository('AppBundle:Sexe');
    	$this->repositoryAnimalType = $this->em->getRepository('AppBundle:AnimalType');
    	$this->repositoryRace = $this->em->getRepository('AppBundle:Race');
    	$this->repositoryClassement = $this->em->getRepository('AppBundle:Classement');
    	$this->repositoryStatut = $this->em->getRepository('AppBundle:Statut');
    	$this->repositoryStatutSortie = $this->em->getRepository('AppBundle:StatutSortie');
    	$this->repositoryNaissanceFacilite = $this->em->getRepository('AppBundle:NaissanceFacilite');
    	$this->repositoryGroupe = $this->em->getRepository('UserBundle:Groupe');
    	$this->container = $containerInterface;
        $this->root = $this->container->get('kernel')->getRootDir();
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();
    }

    public function BovinAccess($groupe_id)
    {
        $groupe = $this->repositoryGroupe->find($groupe_id);

        if($groupe){
            $user_groups = $this->user->getGroups();

            foreach ($user_groups as $value) {
                if($value->getId() == $groupe_id){
                    foreach ($value->getGroupeAnimalTypes() as $animal) {
                        if($animal->getAnimalTypeNom() == "Bovin"){
                            return true;
                        }
                    }
                }
            }
        }
    	


    	return false;
    }

    public function SearchGroupById($groupe_id)
    {
        $groupe = $this->repositoryGroupe->find($groupe_id);

        return $groupe;
    }

    // Optimization function


    public function SelectBovinBySexe($groupe_id, $sexe_code)
    {
        $sexe = $this->repositorySexe->findOneBy(['sexeCode'=>$sexe_code]);
        $groupe = $this->repositoryGroupe->find($groupe_id);
        $bovin = $this->repositoryBovin->findBy(['bovinSexe'=>$sexe,'bovinGroupe'=>$groupe]);
        return $bovin;
    }

    public function SelectBovinByNumberForFiche($groupe_id, $numero, $sexe_code)
    {
        $jumeaux = array();
        $count_jumeaux = 0;
        $count_veaux = 0;
        $moyenne_entre_age = array();
        $first_tour = true;
        $moyenne_entre_age_type = false;
        $tab = array();
        $groupe = $this->repositoryGroupe->find($groupe_id);
        $sexe = $sexe = $this->repositorySexe->findOneBy(['sexeCode'=>$sexe_code]);
        $bovin = $this->repositoryBovin->findOneBy(['bovinNumero'=>$numero,'bovinGroupe'=>$groupe,'bovinSexe'=>$sexe]);

        if(!$bovin){
            return false;
        }

        if($sexe_code == 'F'){
            $veaux = $this->repositoryBovin->findBy(['bovinGroupe'=>$groupe,'bovinMere'=>$bovin],['bovinNaissance' => 'asc']);
        }else{
            $veaux = $this->repositoryBovin->findBy(['bovinGroupe'=>$groupe,'bovinPere'=>$bovin],['bovinNaissance' => 'asc']);
        }

        foreach ($veaux as $key => $veau) {
            $jumeaux[$key] = $veau->getBovinNaissance()->format('y-m-d');
            $moyenne_entre_age[$key] = 0;
            if(!$first_tour){
                $date_naissance->add(new \DateInterval('P1M'));
                while ($date_naissance <= $veau->getBovinNaissance()){
                    $moyenne_entre_age[$key] ++;
                    $moyenne_entre_age_type = true;
                    $date_naissance->add(new \DateInterval('P1M'));
                }
            }
            $date_naissance = $veau->getBovinNaissance();
            $first_tour = false;
            $count_veaux ++;
        }

        if($moyenne_entre_age_type){
            $moyenne_entre_age = array_filter($moyenne_entre_age);
            $moyenne_entre_age = array_sum($moyenne_entre_age) / count($moyenne_entre_age);
            $tab['moyAgeVelage'] = $moyenne_entre_age;
        }else{
            $tab['moyAgeVelage'] = 0;
        }
        

        $jumeaux = array_count_values($jumeaux);

        foreach ($jumeaux as $value) {
            if($value >= 2){
                $count_jumeaux ++;
            }
        }

        $tab['bovin'] = $bovin;
        $tab['nbrJumeaux'] = $count_jumeaux;
        
        $tab['veaux'] = $veaux;
        $tab['nbrVeaux'] = $count_veaux;

        return $tab;
    }

    public function SelectBovinModifSelect()
    {
        $tab_select = array();
        $tab_select['race'] = $this->repositoryRace->findBy(['raceType'=>1]);
        $tab_select['naissanceFacilite'] = $this->repositoryNaissanceFacilite->findAll();
        $tab_select['classement'] = $this->repositoryClassement->findAll();
        $tab_select['statut'] = $this->repositoryStatut->findAll();
        $tab_select['statutSortie'] = $this->repositoryStatutSortie->findAll();
        $tab_select['sexe'] = $this->repositorySexe->findAll();

        return $tab_select;
    }

    public function SelectBovinByNumber($groupe_id, $numero, $id=null)
    {
        $groupe = $this->repositoryGroupe->find($groupe_id);
        $bovin = $this->repositoryBovin->findOneBy(['bovinNumero'=>$numero,'bovinGroupe'=>$groupe]);

        if($bovin){
            if ($id == $bovin->getId()) {
                $bovin ="";
            }
        }
        
    
        return $bovin;
    }

    public function AjoutBovin($groupe_id, $data, $sexe_code,$bovin_exist=null)
    {

        $groupe = $this->repositoryGroupe->find($groupe_id);
        $sexe = $this->repositorySexe->findOneBy(['sexeCode'=>$sexe_code]);
        $classement = $this->repositoryClassement->find($data['bovinClassement']);
        $statut = $this->repositoryStatut->find(3);

        $race = $this->repositoryRace->findOneBy(['raceCode'=>$data['bovinRace'],'raceType'=>1]);
       
        $naissance = new \DateTime($data['bovinNaissance']);

        if(!$bovin_exist){
            $bovin = new Bovin();
        }else{
            $bovin = $this->repositoryBovin->findOneBy(['bovinNumero'=>$bovin_exist]);
        }
        
        if($data['bovinNom'] == ""){
            $bovin->setBovinNom(null);
        }else{
            $bovin->setBovinNom($data['bovinNom']);
        }

        if(isset($data['bovinPere'])){
            $bovin->setBovinPere($data['bovinPere']);
        }

        if(isset($data['bovinMere'])){
            $bovin->setBovinMere($data['bovinMere']);
        }

        $bovin->setBovinNumero($data['bovinNumero']);
        $bovin->setBovinNaissance($naissance);
        $bovin->setBovinGroupe($groupe);
        $bovin->setBovinSexe($sexe);
        $bovin->setBovinStatut($statut);
        $bovin->setBovinRace($race);
        $bovin->setBovinClassement($classement);

        if(isset($data['bovinNaissanceFacilite'])){
            $naissance_facilite = $this->repositoryNaissanceFacilite->find($data['bovinNaissanceFacilite']);
            $bovin->setBovinNaissanceFacilite($naissance_facilite);
        }

        $this->em->persist($bovin);
        $this->em->flush();

        return $bovin;
    }

    public function ModifBovin($data)
    {
        $classement = $this->repositoryClassement->find($data->get('bovinClassement'));
        $race = $this->repositoryRace->find($data->get('bovinRace'));
        $naissance_facilite = $this->repositoryNaissanceFacilite->find($data->get('bovinNaissanceFacilite'));
        $naissance = new \DateTime($data->get('bovinNaissance'));

        if ($data->get('bovinStatutSortie')) {
            $statut_sortie = $this->repositoryStatutSortie->find($data->get('bovinStatutSortie'));
        }else{
            $statut_sortie = null;
        }

        if ($data->get('bovinSortie')) {
            $sortie = new \DateTime($data->get('bovinSortie'));
        }else{
            $sortie = null;
        }

        $bovin = $this->repositoryBovin->find($data->get('bovinId'));

        if($data->get('bovinNom') == ""){
            $bovin->setBovinNom(null);
        }else{
            $bovin->setBovinNom($data->get('bovinNom'));
        }

        $bovin->setBovinNumero($data->get('bovinNumero'));
        $bovin->setBovinNaissance($naissance);
        $bovin->setBovinRace($race);
        $bovin->setBovinClassement($classement);
        $bovin->setBovinNaissanceFacilite($naissance_facilite);
        $bovin->setBovinSortie($sortie);
        $bovin->setBovinStatutSortie($statut_sortie);

        $this->em->persist($bovin);
        $this->em->flush();

        return;
    }

    //=====================


    public function ImportBovinXls($phpExcelObject,$groupe_id)
    {
        $i = 3;
        $error = array();
        $data = array();
        $bovinAdd = array();
        $isError = false;
        $setError = false;
        $appmanager = $this->container->get('appbundle.appmanager');

        $phpExcelGetActiveSheet = $phpExcelObject->getActiveSheet();

        foreach ($phpExcelGetActiveSheet->getRowIterator() as $row) {

            // Colonne Numéro

            $number = $phpExcelGetActiveSheet->getCell("A".$i)->getValue();
            $bovin = $this->repositoryBovin->findOneBy(['bovinNumero'=>$number]);
            if(empty($number)){
                $isError = true;
                $error['NumeroVide'][$i] = true;
            }else{
                $error['NumeroVide'][$i] = false;
            }

            if(!is_numeric($number)){
                $isError = true;
                $error['NumeroNoNumber'][$i] = true;
            }else{
                $error['NumeroNoNumber'][$i] = false;
            }

            if(strlen($number)!= 4){
                $isError = true;
                $error['NumeroTaille'][$i] = true;
            }else{
                $error['NumeroTaille'][$i] = false;
            }

            if($bovin){
                $isError = true;
                $error['NumeroExist'][$i] = true;
            }else{
                $error['NumeroExist'][$i] = false;
            }

            // Colonne Nom

            $nom = $phpExcelGetActiveSheet->getCell("B".$i)->getValue();

            // Colonne Naissance

            if(empty($phpExcelGetActiveSheet->getCell("C".$i)->getValue())){
                $isError = true;
                $error['NaissanceVide'][$i] = true;
            }else{
                $error['NaissanceVide'][$i] = false;
            }

            if(!is_numeric($phpExcelGetActiveSheet->getCell("C".$i)->getValue())){
                $isError = true;
                $error['NaissanceNoDate'][$i] = true;
            }else{
                $naissance = \PHPExcel_Shared_Date::ExcelToPHP($phpExcelGetActiveSheet->getCell("C".$i)->getValue());
                $error['NaissanceNoDate'][$i] = false;
            }

            // Colonne Sexe

            $sexe = strtoupper ($phpExcelGetActiveSheet->getCell("D".$i)->getValue());

            if(empty($sexe)){
                $isError = true;
                $error['SexeVide'][$i] = true;
            }else{
                $error['SexeVide'][$i] = false;
            }

            if($sexe != "M" && $sexe != "F"){
                $isError = true;
                $error['SexeNoCode'][$i] = true;
            }else{
                $error['SexeNoCode'][$i] = false;
            }

            // Colonne Père

            $pere = $phpExcelGetActiveSheet->getCell("E".$i)->getValue();

            if(!empty($pere)){
                if(!is_numeric($pere)){
                    $isError = true;
                    $error['PereNoNumber'][$i] = true;
                }else{
                    $error['PereNoNumber'][$i] = false;
                }
            }else{
                $error['PereNoNumber'][$i] = false;
            }
            
            if(!empty($pere)){
                if(strlen ($pere) != 4){
                    $isError = true;
                    $error['PereTaille'][$i] = true;
                }else{
                    $error['PereTaille'][$i] = false;
                }
            }else{
                $error['PereTaille'][$i] = false;
            }

            // Colonne Mère

            $mere = $phpExcelGetActiveSheet->getCell("F".$i)->getValue();

            if(!empty($mere)){
                if(!is_numeric($mere)){
                    $isError = true;
                    $error['MereNoNumber'][$i] = true;
                }else{
                    $error['MereNoNumber'][$i] = false;
                }
            }else{
                $error['MereNoNumber'][$i] = false;
            }
            
            if(!empty($mere)){
                if(strlen ($mere) != 4){
                    $isError = true;
                    $error['MereTaille'][$i] = true;
                }else{
                    $error['MereTaille'][$i] = false;
                }
            }else{
                $error['MereTaille'][$i] = false;
            }


            $race = $phpExcelGetActiveSheet->getCell("G".$i)->getValue();
            $searchRace = $this->repositoryRace->findOneBy(['raceCode'=>$phpExcelGetActiveSheet->getCell("G".$i)->getValue(),'raceType'=>1]);

            if(empty($race)){
                $isError = true;
                $error['RaceVide'][$i] = true;
            }else{
                $error['RaceVide'][$i] = false;
            }

            if(!is_numeric($race)){
                $isError = true;
                $error['RaceNoNumber'][$i] = true;
            }else{
                $error['RaceNoNumber'][$i] = false;
            }

            if(!$searchRace){
                $isError = true;
                $error['RaceNoExist'][$i] = true;
            }else{
                $error['RaceNoExist'][$i] = false;
            }

            if($error['NumeroVide'][$i] && $error['NaissanceVide'][$i] && $error['SexeVide'][$i] && $error['RaceVide'][$i]){
                $rowEnd = $i - 1;
                $rowEmpty = $i;
                $error['RowSize'] = $rowEmpty;
                break;
            }

            $i++;
            $setError = $isError;
        }

        $i = 3;

        

        if(!$setError && $rowEnd > 2){
            foreach ($phpExcelGetActiveSheet->getRowIterator() as $row) {

                $bovin_exist = $this->repositoryBovin->findOneBy(['bovinNumero'=>$phpExcelGetActiveSheet->getCell("A".$i)->getValue()]);
                $pere_id = $phpExcelGetActiveSheet->getCell("E".$i)->getValue();
                $mere_id = $phpExcelGetActiveSheet->getCell("F".$i)->getValue();

                if($pere_id){
                    $pereInfos = $this->repositoryBovin->findOneBy(['bovinNumero'=>$pere_id]);

                    if(!$pereInfos){

                        $data['pere']['bovinNumero'] = $pere_id;
                        $data['pere']['bovinClassement'] = 1;
                        $data['pere']['bovinStatut'] = 3;
                        $data['pere']['bovinRace'] = -1;
                        $data['pere']['bovinNaissance'] = "01/01/0000";
                        $data['pere']['bovinNom'] = null;

                        $pereInfos = $this->AjoutBovin($groupe_id, $data['pere'], 'M');
                        $bovinAdd['bovinPere'] = $pereInfos;

                    }else{
                        $bovinAdd['bovinPere'] = $pereInfos;
                    }
                }

                if($mere_id){
                    $mereInfos = $this->repositoryBovin->findOneBy(['bovinNumero'=>$mere_id]);

                    if(!$mereInfos){

                        $data['mere']['bovinNumero'] = $pere_id;
                        $data['mere']['bovinClassement'] = 1;
                        $data['mere']['bovinStatut'] = 3;
                        $data['mere']['bovinRace'] = -1;
                        $data['mere']['bovinNaissance'] = "01/01/0000";
                        $data['mere']['bovinNom'] = null;

                        $mereInfos = $this->AjoutBovin($groupe_id, $data['mere'], 'F');
                        $bovinAdd['bovinMere'] = $mereInfos;
                        
                    }else{
                        $bovinAdd['bovinMere'] = $mereInfos;
                    }
                }
            

                $bovinAdd['bovinNumero'] = $phpExcelGetActiveSheet->getCell("A".$i)->getValue();
                $bovinAdd['bovinClassement'] = 1;
                $bovinAdd['bovinStatut'] = 5;
                $bovinAdd['bovinRace'] = $phpExcelGetActiveSheet->getCell("G".$i)->getValue();
                $bovinAdd['bovinNaissance'] = \PHPExcel_Style_NumberFormat::toFormattedString($phpExcelGetActiveSheet->getCell("C".$i)->getValue(), 'YYYY-MM-DD');
                $bovinAdd['bovinNom'] = $phpExcelGetActiveSheet->getCell("B".$i)->getValue();


                $this->AjoutBovin($groupe_id, $bovinAdd, strtoupper($phpExcelGetActiveSheet->getCell("D".$i)->getValue()),
                    $bovin_exist);


                if($rowEnd == $i){
                    break;
                }

                $i ++;

            }
        }else{
            return $error;
        }


    }
    
}