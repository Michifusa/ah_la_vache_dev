<?php

namespace BovinBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use BovinBundle\Entity\Bovin;
use BovinBundle\Form\VacheType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;



class VacheController extends Controller
{

    /**
     * @Route("/vache", name="vache")
     * @Method({"GET"})
     */
    public function BovinVachesPageAction($groupe,Request $request)
    {
    	$bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
    	if($bovin_access){
            $form = $this->get('form.factory')->create(VacheType::class);

    		return $this->render('BovinBundle:Vaches:vaches_page.html.twig',['form' => $form->createView()]);
    	}else{

            return new RedirectResponse($this->generateUrl('accueil'));
    	}
        
    }

    /**
     * @Route("/vache/numero/{numero}", name="fiche_vache", options={"expose"=true}, requirements={"numero"="\d+"})
     * @Method({"GET"})
     */
    public function BovinVacheFicheAction($groupe,Request $request,$numero)
    {
    	$error_numero = $request->query->get('error_numero');

    	$bovinmanager = $this->container->get('bovinbundle.bovinmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
    	if($bovin_access){
    		$vache = $bovinmanager->SelectBovinByNumberForFiche($groupe,$numero,'F');

            if($vache){
                $liste_select = $bovinmanager->SelectBovinModifSelect();
                return $this->render('BovinBundle:Vaches:fiche_vache.html.twig',['vacheInfos'=>$vache,'listeSelect'=>$liste_select,'error_numero'=>$error_numero]);
            }else{
                return new RedirectResponse($this->generateUrl('vache',['groupe'=>$groupe]));
            }
            
    		
    	}else{
            return new RedirectResponse($this->generateUrl('accueil'));
    	}
        
    }

    /**
     * @Route("/vache/load-table", name="vache_load_table", options={"expose"=true})
     * @Method({"GET"})
     */
    public function BovinVacheTableLoadAction($groupe,Request $request)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            $liste_vache = $bovinmanager->SelectBovinBySexe($groupe,'F');
            return $this->render('BovinBundle:Vaches/Table:table_list_vache.html.twig',['liste_vache'=>$liste_vache]);
        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/vache/ajouter", name="vache_ajout", options={"expose"=true})
     * @Method({"POST"})
     */
    public function BovinVacheAjoutAction($groupe,Request $request)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
        	$check_numero = $bovinmanager->SelectBovinByNumber($groupe,$request->request->get('bovinbundle_bovin')['bovinNumero']);

        	if($check_numero ==""){
        		$bovin = $bovinmanager->AjoutBovin($groupe,$request->request->get('bovinbundle_bovin'),'F');
            	return new Response($bovin->getBovinNumero());
        	}else{
        		return new Response('erreur_numero');
        	}

        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/vache/numero/{numero}/ajout-image", options={"expose"=true}, name="fiche_vache_ajout_image", requirements={"numero"="\d+"})
     * @Method({"POST"})
     */
    public function BovinVacheAddPictureAction($groupe,Request $request,$numero)
    {
    	$bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $appmanager = $this->container->get('appbundle.appmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
    	if($bovin_access){
    		$vache = $bovinmanager->SelectBovinByNumber($groupe,$numero);
    		$groupe = $bovinmanager->SearchGroupById($groupe);
        	$path = $this->get('kernel')->getRootDir().'/../web/upload/groupe/'.$groupe->getId().'/';

    		switch ($request->query->get('type')) {
    			case 'bovinPictureFace':

                        $image_path = $appmanager->UploadImageOnServer($path, $vache->getBovinPictureFacePath(), $request->request->get('value'));
                        $vache->setBovinPictureFacePath($image_path.'.png');

    				break;
    			
    			case 'bovinPictureCote':

		    			$image_path = $appmanager->UploadImageOnServer($path, $vache->getBovinPictureCotePath(), $request->request->get('value'));
                        $vache->setBovinPictureCotePath($image_path.'.png');

    				break;
    		}

    		$this->getDoctrine()->getManager()->persist($vache);
        	$this->getDoctrine()->getManager()->flush();

    		return new RedirectResponse($this->generateUrl('fiche_vache',['groupe'=>$groupe->getId(),'numero'=>$numero]));

    	}else{

            return new RedirectResponse($this->generateUrl('accueil'));
    	}
        
    }

    /**
     * @Route("/vache/numero/{numero}/modif-fiche", options={"expose"=true}, name="fiche_vache_modification", requirements={"numero"="\d+"})
     * @Method({"POST"})
     */
    public function BovinVacheModifFicheAction($groupe,Request $request,$numero)
    {
    	$bovinmanager = $this->container->get('bovinbundle.bovinmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
        	$check_numero = $bovinmanager->SelectBovinByNumber($groupe,$request->request->get('bovinNumero'),$request->request->get('bovinId'));

            if($check_numero ==""){

            	$bovinmanager->ModifBovin($request->request);

	            return new RedirectResponse($this->generateUrl('fiche_vache', ['groupe'=>$groupe,'numero' => $request->request->get('bovinNumero')]));

            }else{

            	return new RedirectResponse($this->generateUrl('fiche_vache', ['groupe'=>$groupe,'numero' => $numero,'error_numero' => true]));

            }

        }else{
        	return new RedirectResponse($this->generateUrl('accueil'));
        }
    }
}
