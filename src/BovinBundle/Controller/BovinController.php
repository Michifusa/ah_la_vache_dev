<?php

namespace BovinBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use BovinBundle\Entity\Bovin;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;



class BovinController extends Controller
{
    /**
     * @Route("/", name="bovin")
     * @Method({"GET"})
     */
    public function BovinPageAction($groupe,Request $request)
    {
        
    	$bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);

    	if($bovin_access){
            $tab_select = $bovinmanager->SelectBovinModifSelect();
    		return $this->render('BovinBundle:Bovin:index.html.twig',['tab_select'=>$tab_select]);
    	}else{
            return new RedirectResponse($this->generateUrl('accueil'));
    	}
        
    }

    /**
     * @Route("/import-bovin-xls", name="bovin_ajout_xls", options={"expose"=true})
     * @Method({"POST"})
     */
    public function BovinAjoutImportXLSBovinAction($groupe,Request $request)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            $appmanager = $this->container->get('appbundle.appmanager');
            if ($request->files->get('xls_file')) {
                $excel_type = $appmanager->PhpExcelFile($request->files->get('xls_file')->getMimeType());
            }else{
                $excel_type = "no_file";
            }
            
            if($excel_type){

                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($request->files->get('xls_file'));

                $importXlsBovin = $bovinmanager->ImportBovinXls($phpExcelObject,$groupe);
                return new JsonResponse($importXlsBovin);
            }else{
               return new Response('error');
            }
        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }
}
