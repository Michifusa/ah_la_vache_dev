<?php

namespace BovinBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use BovinBundle\Entity\Bovin;
use BovinBundle\Form\TaureauType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;



class TaureauController extends Controller
{
   /**
     * @Route("/taureau", name="taureau")
     * @Method({"GET"})
     */
    public function BovinTaureauxPageAction($groupe,Request $request)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            $form = $this->get('form.factory')->create(TaureauType::class);

            return $this->render('BovinBundle:Taureau:taureau_pages.html.twig',['form' => $form->createView()]);
        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/taureau/numero/{numero}", name="fiche_taureau", options={"expose"=true}, requirements={"numero"="\d+"})
     * @Method({"GET"})
     */
    public function BovinTaureauFicheAction($groupe,Request $request,$numero)
    {
        $error_numero = $request->query->get('error_numero');
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            $taureau = $bovinmanager->SelectBovinByNumberForFiche($groupe, $numero, 'M');

            if($taureau){
                $liste_select = $bovinmanager->SelectBovinModifSelect();
                return $this->render('BovinBundle:Taureau:fiche_taureau.html.twig',['taureauInfos'=>$taureau,'listeSelect'=>$liste_select,'error_numero'=>$error_numero]);
            }else{
                return new RedirectResponse($this->generateUrl('taureau',['groupe'=>$groupe]));
            }

        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/taureau/load-table", name="taureau_load_table", options={"expose"=true})
     * @Method({"GET"})
     */
    public function BovinTaureauxTableLoadAction($groupe,Request $request)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            $liste_taureau = $bovinmanager->SelectBovinBySexe($groupe,'M');
            return $this->render('BovinBundle:Taureau/Table:table_list_taureau.html.twig',['liste_taureau'=>$liste_taureau]);
        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/taureau/ajouter", name="taureau_ajout", options={"expose"=true})
     * @Method({"POST"})
     */
    public function BovinTaureauAjoutAction($groupe,Request $request)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
                
            $check_numero = $bovinmanager->SelectBovinByNumber($groupe,$request->request->get('bovinbundle_bovin')['bovinNumero']);

            if($check_numero ==""){
                $bovin = $bovinmanager->AjoutBovin($groupe,$request->request->get('bovinbundle_bovin'),'M');
                return new Response($bovin->getBovinNumero());
            }else{
                return new Response('erreur_numero');
            }

        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/taureau/numero/{numero}/ajout-image", options={"expose"=true}, name="fiche_taureau_ajout_image", requirements={"numero"="\d+"})
     * @Method({"POST"})
     */
    public function BovinTaureauAddPictureAction($groupe,Request $request,$numero)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');
        $appmanager = $this->container->get('appbundle.appmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            
            $taureau = $bovinmanager->SelectBovinByNumber($groupe,$numero);
            $groupe = $bovinmanager->SearchGroupById($groupe);
            $path = $this->get('kernel')->getRootDir().'/../web/upload/groupe/'.$groupe->getId().'/';

            switch ($request->query->get('type')) {
                case 'bovinPictureFace':

                        $image_path = $appmanager->UploadImageOnServer($path, $taureau->getBovinPictureFacePath(), $request->request->get('value'));
                        $taureau->setBovinPictureFacePath($image_path.'.png');

                    break;
                
                case 'bovinPictureCote':

                        $image_path = $appmanager->UploadImageOnServer($path, $taureau->getBovinPictureFacePath(), $request->request->get('value'));
                        $taureau->setBovinPictureFacePath($image_path.'.png');

                    break;
            }

            $this->getDoctrine()->getManager()->persist($taureau);
            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('fiche_taureau',['groupe'=>$groupe->getId(),'numero'=>$numero]));

        }else{

            return new RedirectResponse($this->generateUrl('accueil'));
        }
        
    }

    /**
     * @Route("/taureau/numero/{numero}/modif-fiche", options={"expose"=true}, name="fiche_taureau_modification", requirements={"numero"="\d+"})
     * @Method({"POST"})
     */
    public function BovinTaureauModifFicheAction($groupe,Request $request,$numero)
    {
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');

        $bovin_access = $bovinmanager->BovinAccess($groupe);
        
        if($bovin_access){
            $check_numero = $bovinmanager->SelectBovinByNumber($groupe,$request->request->get('bovinNumero'),$request->request->get('bovinId'));

            if($check_numero ==""){

                $bovinmanager->ModifBovin($request->request);

                return new RedirectResponse($this->generateUrl('fiche_taureau', ['groupe'=>$groupe,'numero' => $request->request->get('bovinNumero')]));

            }else{

                return new RedirectResponse($this->generateUrl('fiche_taureau', ['groupe'=>$groupe,'numero' => $numero,'error_numero' => true]));

            }

        }else{
            return new RedirectResponse($this->generateUrl('accueil'));
        }
    }
}
