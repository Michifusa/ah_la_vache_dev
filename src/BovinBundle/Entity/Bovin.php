<?php

namespace BovinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bovin
 *
 * @ORM\Table(name="bovin")
 * @ORM\Entity(repositoryClass="BovinBundle\Repository\BovinRepository")
 */
class Bovin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Bovin_nom", type="string", length=255, nullable=true)
     */
    private $bovinNom;

    /**
     * @var int
     *
     * @ORM\Column(name="Bovin_numero", type="integer")
     */
    private $bovinNumero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Bovin_naissance", type="datetime")
     */
    private $bovinNaissance;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Groupe", inversedBy="groupeBovin")
    * @ORM\JoinColumn(name="Bovin_groupe", referencedColumnName="id")
    * 
    */
    private $bovinGroupe;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sexe", inversedBy="sexeBovin")
    * @ORM\JoinColumn(name="Bovin_sexe", referencedColumnName="id")
    * 
    */
    private $bovinSexe;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Statut", inversedBy="statutBovin")
    * @ORM\JoinColumn(name="Bovin_statut", referencedColumnName="id")
    * 
    */
    private $bovinStatut;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Race", inversedBy="raceBovin")
    * @ORM\JoinColumn(name="Bovin_race", referencedColumnName="id")
    * 
    */
    private $bovinRace;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Classement", inversedBy="classementBovin")
    * @ORM\JoinColumn(name="Bovin_classement", referencedColumnName="id")
    * 
    */
    private $bovinClassement;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="Bovin_sortie", type="datetime",nullable=true)
    *
    * 
    */
    private $bovinSortie;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=2,nullable=true)
     *
     */
    private $bovinPoidsCarcasse;

    /**
     *
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinPere")
     *
     */
    private $bovinParentPere;

    /**
     * @var INT
     *
     * @ORM\ManyToOne(targetEntity="BovinBundle\Entity\Bovin", inversedBy="bovinParentPere")
     * @ORM\JoinColumn(name="Bovin_Pere", referencedColumnName="id")
     *
     */
    private $bovinPere;

    /**
     *
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinMere")
     *
     */
    private $bovinParentMere;

    /**
     * @var INT
     *
     * @ORM\ManyToOne(targetEntity="BovinBundle\Entity\Bovin", inversedBy="bovinParentMere")
     * @ORM\JoinColumn(name="Bovin_Mere", referencedColumnName="id")
     *
     */
    private $bovinMere;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StatutSortie", inversedBy="statutSortieBovin")
    * @ORM\JoinColumn(name="Bovin_statut_sortie", referencedColumnName="id")
    * 
    */
    private $bovinStatutSortie;

    /**
     * @var INT
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\NaissanceFacilite", inversedBy="naissanceFaciliteBovin")
     * @ORM\JoinColumn(name="Bovin_naissance_facilite", referencedColumnName="id")
     *
     */
    private $bovinNaissanceFacilite;

    /**
     * @var string
     *
     * @ORM\Column(name="Bovin_picture_face_path", type="string", length=255, nullable=true)
     */
    private $bovinPictureFacePath;

    /**
     * @var string
     *
     * @ORM\Column(name="Bovin_picture_cote_path", type="string", length=255, nullable=true)
     */
    private $bovinPictureCotePath;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bovinNom
     *
     * @param string $bovinNom
     *
     * @return Bovin
     */
    public function setBovinNom($bovinNom)
    {
        $this->bovinNom = $bovinNom;

        return $this;
    }

    /**
     * Get bovinNom
     *
     * @return string
     */
    public function getBovinNom()
    {
        return $this->bovinNom;
    }

    /**
     * Set bovinNumero
     *
     * @param integer $bovinNumero
     *
     * @return Bovin
     */
    public function setBovinNumero($bovinNumero)
    {
        $this->bovinNumero = $bovinNumero;

        return $this;
    }

    /**
     * Get bovinNumero
     *
     * @return int
     */
    public function getBovinNumero()
    {
        return $this->bovinNumero;
    }

    /**
     * Set bovinNaissance
     *
     * @param \DateTime $bovinNaissance
     *
     * @return Bovin
     */
    public function setBovinNaissance($bovinNaissance)
    {
        $this->bovinNaissance = $bovinNaissance;

        return $this;
    }

    /**
     * Get bovinNaissance
     *
     * @return \DateTime
     */
    public function getBovinNaissance()
    {
        return $this->bovinNaissance;
    }

    /**
     * Set bovinGroupe
     *
     * @param \UserBundle\Entity\Groupe $bovinGroupe
     *
     * @return Bovin
     */
    public function setBovinGroupe(\UserBundle\Entity\Groupe $bovinGroupe = null)
    {
        $this->bovinGroupe = $bovinGroupe;

        return $this;
    }

    /**
     * Get bovinGroupe
     *
     * @return \UserBundle\Entity\Groupe
     */
    public function getBovinGroupe()
    {
        return $this->bovinGroupe;
    }

    /**
     * Set bovinSexe
     *
     * @param \AppBundle\Entity\Sexe $bovinSexe
     *
     * @return Bovin
     */
    public function setBovinSexe(\AppBundle\Entity\Sexe $bovinSexe = null)
    {
        $this->bovinSexe = $bovinSexe;

        return $this;
    }

    /**
     * Get bovinSexe
     *
     * @return \AppBundle\Entity\Sexe
     */
    public function getBovinSexe()
    {
        return $this->bovinSexe;
    }

    /**
     * Set bovinStatut
     *
     * @param \AppBundle\Entity\Statut $bovinStatut
     *
     * @return Bovin
     */
    public function setBovinStatut(\AppBundle\Entity\Statut $bovinStatut = null)
    {
        $this->bovinStatut = $bovinStatut;

        return $this;
    }

    /**
     * Get bovinStatut
     *
     * @return \AppBundle\Entity\Statut
     */
    public function getBovinStatut()
    {
        return $this->bovinStatut;
    }

    /**
     * Set bovinRace
     *
     * @param \AppBundle\Entity\Race $bovinRace
     *
     * @return Bovin
     */
    public function setBovinRace(\AppBundle\Entity\Race $bovinRace = null)
    {
        $this->bovinRace = $bovinRace;

        return $this;
    }

    /**
     * Get bovinRace
     *
     * @return \AppBundle\Entity\Race
     */
    public function getBovinRace()
    {
        return $this->bovinRace;
    }

    /**
     * Set bovinClassement
     *
     * @param \AppBundle\Entity\Classement $bovinClassement
     *
     * @return Bovin
     */
    public function setBovinClassement(\AppBundle\Entity\Classement $bovinClassement = null)
    {
        $this->bovinClassement = $bovinClassement;

        return $this;
    }

    /**
     * Get bovinClassement
     *
     * @return \AppBundle\Entity\Classement
     */
    public function getBovinClassement()
    {
        return $this->bovinClassement;
    }

    /**
     * Get bovinAge
     *
     * @return int
     */
    public function getBovinAge()
    {
        $this->date = new \DateTime();

        $this->bovinAge = 0;

        $this->bovinNaissance->add(new \DateInterval('P1M'));
        while ($this->bovinNaissance <= $this->date){
            $this->bovinAge ++;
            $this->bovinNaissance->add(new \DateInterval('P1M'));
        }


        return $this->bovinAge;
    }

    /**
     * Set bovinPoidsCarcasse
     *
     * @param string $bovinPoidsCarcasse
     *
     * @return Bovin
     */
    public function setBovinPoidsCarcasse($bovinPoidsCarcasse)
    {
        $this->bovinPoidsCarcasse = $bovinPoidsCarcasse;
    
        return $this;
    }

    /**
     * Get bovinPoidsCarcasse
     *
     * @return string
     */
    public function getBovinPoidsCarcasse()
    {
        return $this->bovinPoidsCarcasse;
    }

    /**
     * Set bovinSortie
     *
     * @param \DateTime $bovinSortie
     *
     * @return Bovin
     */
    public function setBovinSortie($bovinSortie)
    {
        $this->bovinSortie = $bovinSortie;
    
        return $this;
    }

    /**
     * Get bovinSortie
     *
     * @return \DateTime
     */
    public function getBovinSortie()
    {
        return $this->bovinSortie;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bovinParentPere = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bovinParentMere = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bovinParentPere
     *
     * @param \BovinBundle\Entity\Bovin $bovinParentPere
     *
     * @return Bovin
     */
    public function addBovinParentPere(\BovinBundle\Entity\Bovin $bovinParentPere)
    {
        $this->bovinParentPere[] = $bovinParentPere;
    
        return $this;
    }

    /**
     * Remove bovinParentPere
     *
     * @param \BovinBundle\Entity\Bovin $bovinParentPere
     */
    public function removeBovinParentPere(\BovinBundle\Entity\Bovin $bovinParentPere)
    {
        $this->bovinParentPere->removeElement($bovinParentPere);
    }

    /**
     * Get bovinParentPere
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBovinParentPere()
    {
        return $this->bovinParentPere;
    }

    /**
     * Set bovinPere
     *
     * @param \BovinBundle\Entity\Bovin $bovinPere
     *
     * @return Bovin
     */
    public function setBovinPere(\BovinBundle\Entity\Bovin $bovinPere = null)
    {
        $this->bovinPere = $bovinPere;
    
        return $this;
    }

    /**
     * Get bovinPere
     *
     * @return \BovinBundle\Entity\Bovin
     */
    public function getBovinPere()
    {
        return $this->bovinPere;
    }

    /**
     * Add bovinParentMere
     *
     * @param \BovinBundle\Entity\Bovin $bovinParentMere
     *
     * @return Bovin
     */
    public function addBovinParentMere(\BovinBundle\Entity\Bovin $bovinParentMere)
    {
        $this->bovinParentMere[] = $bovinParentMere;
    
        return $this;
    }

    /**
     * Remove bovinParentMere
     *
     * @param \BovinBundle\Entity\Bovin $bovinParentMere
     */
    public function removeBovinParentMere(\BovinBundle\Entity\Bovin $bovinParentMere)
    {
        $this->bovinParentMere->removeElement($bovinParentMere);
    }

    /**
     * Get bovinParentMere
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBovinParentMere()
    {
        return $this->bovinParentMere;
    }

    /**
     * Set bovinMere
     *
     * @param \BovinBundle\Entity\Bovin $bovinMere
     *
     * @return Bovin
     */
    public function setBovinMere(\BovinBundle\Entity\Bovin $bovinMere = null)
    {
        $this->bovinMere = $bovinMere;
    
        return $this;
    }

    /**
     * Get bovinMere
     *
     * @return \BovinBundle\Entity\Bovin
     */
    public function getBovinMere()
    {
        return $this->bovinMere;
    }

    /**
     * Set bovinStatutSortie
     *
     * @param \AppBundle\Entity\StatutSortie $bovinStatutSortie
     *
     * @return Bovin
     */
    public function setBovinStatutSortie(\AppBundle\Entity\StatutSortie $bovinStatutSortie = null)
    {
        $this->bovinStatutSortie = $bovinStatutSortie;

        return $this;
    }

    /**
     * Get bovinStatutSortie
     *
     * @return \AppBundle\Entity\StatutSortie
     */
    public function getBovinStatutSortie()
    {
        return $this->bovinStatutSortie;
    }



    /**
     * Set bovinNaissanceFacilite
     *
     * @param \AppBundle\Entity\NaissanceFacilite $bovinNaissanceFacilite
     *
     * @return Bovin
     */
    public function setBovinNaissanceFacilite(\AppBundle\Entity\NaissanceFacilite $bovinNaissanceFacilite = null)
    {
        $this->bovinNaissanceFacilite = $bovinNaissanceFacilite;

        return $this;
    }

    /**
     * Get bovinNaissanceFacilite
     *
     * @return \AppBundle\Entity\NaissanceFacilite
     */
    public function getBovinNaissanceFacilite()
    {
        return $this->bovinNaissanceFacilite;
    }

    /**
     * Set bovinPictureFacePath
     *
     * @param string $bovinPictureFacePath
     *
     * @return Bovin
     */
    public function setBovinPictureFacePath($bovinPictureFacePath)
    {
        $this->bovinPictureFacePath = $bovinPictureFacePath;

        return $this;
    }

    /**
     * Get bovinPictureFacePath
     *
     * @return string
     */
    public function getBovinPictureFacePath()
    {
        return $this->bovinPictureFacePath;
    }

    /**
     * Set bovinPictureCotePath
     *
     * @param string $bovinPictureCotePath
     *
     * @return Bovin
     */
    public function setBovinPictureCotePath($bovinPictureCotePath)
    {
        $this->bovinPictureCotePath = $bovinPictureCotePath;

        return $this;
    }

    /**
     * Get bovinPictureCotePath
     *
     * @return string
     */
    public function getBovinPictureCotePath()
    {
        return $this->bovinPictureCotePath;
    }
}
