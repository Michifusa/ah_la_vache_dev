<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class PDFController extends Controller
{
	/**
    * @Route("/generate-pdf", name="generate_pdf", options={"expose"=true})
    */
    public function CreatePDFAction(Request $request)
    {
        
    	// Commande JQuery pour voir pdf
    	// $('#modalPDFViewer .modal-body').append('<iframe id="pdfIframe" src="'+src+'" type="application/pdf" width="100%" height="600" onload="iframe()"></iframe>');

    	$pdfmanager = $this->container->get('appbundle.pdfmanager');
        $pdf_file = $pdfmanager->GeneratePdfBody($request);
        $path = $this->get('kernel')->getRootDir().'/../web/upload/groupe/'.$request->query->get('groupe').'/'.$pdf_file['nom'];
        $url = $request->getBasePath().'/upload/groupe/'.$request->query->get('groupe').'/'.$pdf_file['nom'];

        $snappy = $this->get('knp_snappy.pdf');

        $options = [
		    'margin-top'    => 10,
		    'margin-right'  => 10,
		    'margin-bottom' => 10,
		    'margin-left'   => 10,
		];
        
        $snappy->setOption('footer-html', $pdf_file['footer']);
        $snappy->setOption('header-html', $pdf_file['header']);

        $snappy->setOption('margin-top', 10);
        $snappy->setOption('margin-right', 10);
        $snappy->setOption('margin-bottom', 10);
        $snappy->setOption('margin-left', 10);

        if(file_exists ($path)){
            unlink($path);
        }

        $this->get('knp_snappy.pdf')->generateFromHtml($pdf_file['body'],$path);

        return new Response($url);
    }
    
}