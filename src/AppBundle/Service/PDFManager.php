<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use BovinBundle\Entity\Bovin;

class PDFManager{

	private $entityManager;

    public function __construct(EntityManager $entityManager,ContainerInterface $containerInterface)
    {
    	$this->em = $entityManager;
    	$this->repositoryBovin = $this->em->getRepository('BovinBundle:Bovin');
    	$this->repositorySexe = $this->em->getRepository('AppBundle:Sexe');
    	$this->repositoryAnimalType = $this->em->getRepository('AppBundle:AnimalType');
    	$this->repositoryRace = $this->em->getRepository('AppBundle:Race');
    	$this->repositoryClassement = $this->em->getRepository('AppBundle:Classement');
    	$this->repositoryStatut = $this->em->getRepository('AppBundle:Statut');
    	$this->repositoryStatutSortie = $this->em->getRepository('AppBundle:StatutSortie');
    	$this->repositoryNaissanceFacilite = $this->em->getRepository('AppBundle:NaissanceFacilite');
    	$this->repositoryGroupe = $this->em->getRepository('UserBundle:Groupe');
    	$this->container = $containerInterface;
        $this->root = $this->container->get('kernel')->getRootDir();
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();
        $this->templating = $this->container->get('templating');

    }

    public function GeneratePdfBody($data)
    {
        $pdf_file = array();
        $bovinmanager = $this->container->get('bovinbundle.bovinmanager');

        switch ($data->query->get('type')) {
            case 'pdfBovinTaureau':

                $liste_taureau = $bovinmanager->SelectBovinBySexe($data->query->get('groupe'),'M');

                $pdf_file['nom'] = 'Pdf_Taureau.pdf';

                $pdf_file['header'] = $this->templating->render('AppBundle:PDF:header_pdf.html.twig');
                $pdf_file['body'] = $this->templating->render('AppBundle:PDF:body_bovin_taureau.html.twig',['liste_taureau'=>$liste_taureau]);
                $pdf_file['footer'] = $this->templating->render('AppBundle:PDF:footer_pdf.html.twig');

                break;
            
            case 'pdfBovinVache':

                $liste_vache = $bovinmanager->SelectBovinBySexe($data->query->get('groupe'),'F');

                $pdf_file['nom'] = 'Pdf_Vache.pdf';

                $pdf_file['header'] = $this->templating->render('AppBundle:PDF:header_pdf.html.twig');
                $pdf_file['body'] = $this->templating->render('AppBundle:PDF:body_bovin_vache.html.twig',['liste_vache'=>$liste_vache]);
                $pdf_file['footer'] = $this->templating->render('AppBundle:PDF:footer_pdf.html.twig');

                break;
        }

        return $pdf_file;
    }
}