<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use BovinBundle\Entity\Bovin;

class AppManager{

	private $entityManager;

    public function __construct(EntityManager $entityManager,ContainerInterface $containerInterface)
    {
    	$this->em = $entityManager;
    	$this->repositoryBovin = $this->em->getRepository('BovinBundle:Bovin');
    	$this->repositorySexe = $this->em->getRepository('AppBundle:Sexe');
    	$this->repositoryAnimalType = $this->em->getRepository('AppBundle:AnimalType');
    	$this->repositoryRace = $this->em->getRepository('AppBundle:Race');
    	$this->repositoryClassement = $this->em->getRepository('AppBundle:Classement');
    	$this->repositoryStatut = $this->em->getRepository('AppBundle:Statut');
    	$this->repositoryStatutSortie = $this->em->getRepository('AppBundle:StatutSortie');
    	$this->repositoryNaissanceFacilite = $this->em->getRepository('AppBundle:NaissanceFacilite');
    	$this->repositoryGroupe = $this->em->getRepository('UserBundle:Groupe');
    	$this->container = $containerInterface;
        $this->root = $this->container->get('kernel')->getRootDir();
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();
    }

    public function PhpExcelFile($file_type)
    {
        $excel = false;
            
        switch ($file_type) {
            case 'application/vnd.ms-excel':
                $excel = true;
                break;
            case 'application/vnd.ms-excel.addin.macroEnabled.12':
                $excel = true;
                break;
            case 'application/vnd.ms-excel.sheet.binary.macroEnabled.12':
                $excel = true;
                break;
            case 'application/vnd.ms-excel.sheet.macroEnabled.12':
                $excel = true;
                break;
            case 'application/vnd.ms-excel.template.macroEnabled.12':
                $excel = true;
                break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                $excel = true;
                break;
        }
        return $excel;
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function randomPassword( $length = 12) 
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#&$';
        return substr( str_shuffle( $chars ), 0, $length );
    }

    public function UploadImageOnServer($path, $db_path, $img_url)
    {
        if($db_path){
            if(file_exists ($path.$db_path)){
                unlink($path.$db_path);
            }
        }
        
        $image_path = uniqid().uniqid();

        $image = base64_decode(str_replace('data:image/png;base64,', '', $img_url));

        $fp = fopen($path.$image_path.'.png', 'w');
        
        fwrite($fp, $image);
        fclose($fp);

        return $image_path;
    }
}