<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Race
 *
 * @ORM\Table(name="race")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RaceRepository")
 */
class Race
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Race_code", type="string", length=255)
     */
    private $raceCode;

    /**
     * @var string
     *
     * @ORM\Column(name="Race_nom", type="string", length=255)
     */
    private $raceNom;

    /**
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinRace")
     */
    private $raceBovin;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AnimalType", inversedBy="animalTypeRace")
    * @ORM\JoinColumn(name="Race_type", referencedColumnName="id")
    * 
    */
    private $raceType;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set raceCode
     *
     * @param string $raceCode
     *
     * @return Race
     */
    public function setRaceCode($raceCode)
    {
        $this->raceCode = $raceCode;

        return $this;
    }

    /**
     * Get raceCode
     *
     * @return string
     */
    public function getRaceCode()
    {
        return $this->raceCode;
    }

    /**
     * Set raceNom
     *
     * @param string $raceNom
     *
     * @return Race
     */
    public function setRaceNom($raceNom)
    {
        $this->raceNom = $raceNom;

        return $this;
    }

    /**
     * Get raceNom
     *
     * @return string
     */
    public function getRaceNom()
    {
        return $this->raceNom;
    }


    /**
     * Get raceBovin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRaceBovin()
    {
        return $this->raceBovin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->raceBovin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add raceBovin
     *
     * @param \BovinBundle\Entity\Bovin $raceBovin
     *
     * @return Race
     */
    public function addRaceBovin(\BovinBundle\Entity\Bovin $raceBovin)
    {
        $this->raceBovin[] = $raceBovin;
    
        return $this;
    }

    /**
     * Remove raceBovin
     *
     * @param \BovinBundle\Entity\Bovin $raceBovin
     */
    public function removeRaceBovin(\BovinBundle\Entity\Bovin $raceBovin)
    {
        $this->raceBovin->removeElement($raceBovin);
    }

    /**
     * Set raceType
     *
     * @param \AppBundle\Entity\AnimalType $raceType
     *
     * @return Race
     */
    public function setRaceType(\AppBundle\Entity\AnimalType $raceType = null)
    {
        $this->raceType = $raceType;
    
        return $this;
    }

    /**
     * Get raceType
     *
     * @return \AppBundle\Entity\AnimalType
     */
    public function getRaceType()
    {
        return $this->raceType;
    }
}
