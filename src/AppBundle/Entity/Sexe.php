<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sexe
 *
 * @ORM\Table(name="sexe")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SexeRepository")
 */
class Sexe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Sexe_code", type="string", length=255)
     */
    private $sexeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="Sexe_nom", type="string", length=255)
     */
    private $sexeNom;

    /**
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinSexe")
     */
    private $sexeBovin;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Classement", mappedBy="classementSexe")
     */
    private $sexeClassement;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Statut", mappedBy="statutSexe")
     */
    private $sexeStatut;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sexeCode
     *
     * @param string $sexeCode
     *
     * @return Sexe
     */
    public function setSexeCode($sexeCode)
    {
        $this->sexeCode = $sexeCode;

        return $this;
    }

    /**
     * Get sexeCode
     *
     * @return string
     */
    public function getSexeCode()
    {
        return $this->sexeCode;
    }

    /**
     * Set sexeNom
     *
     * @param string $sexeNom
     *
     * @return Sexe
     */
    public function setSexeNom($sexeNom)
    {
        $this->sexeNom = $sexeNom;

        return $this;
    }

    /**
     * Get sexeNom
     *
     * @return string
     */
    public function getSexeNom()
    {
        return $this->sexeNom;
    }
    
    /**
     * Get sexeBovin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSexeBovin()
    {
        return $this->sexeBovin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sexeBovin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sexeBovin
     *
     * @param \BovinBundle\Entity\Bovin $sexeBovin
     *
     * @return Sexe
     */
    public function addSexeBovin(\BovinBundle\Entity\Bovin $sexeBovin)
    {
        $this->sexeBovin[] = $sexeBovin;
    
        return $this;
    }

    /**
     * Remove sexeBovin
     *
     * @param \BovinBundle\Entity\Bovin $sexeBovin
     */
    public function removeSexeBovin(\BovinBundle\Entity\Bovin $sexeBovin)
    {
        $this->sexeBovin->removeElement($sexeBovin);
    }

    /**
     * Add sexeClassement
     *
     * @param \AppBundle\Entity\Classement $sexeClassement
     *
     * @return Sexe
     */
    public function addSexeClassement(\AppBundle\Entity\Classement $sexeClassement)
    {
        $this->sexeClassement[] = $sexeClassement;
    
        return $this;
    }

    /**
     * Remove sexeClassement
     *
     * @param \AppBundle\Entity\Classement $sexeClassement
     */
    public function removeSexeClassement(\AppBundle\Entity\Classement $sexeClassement)
    {
        $this->sexeClassement->removeElement($sexeClassement);
    }

    /**
     * Get sexeClassement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSexeClassement()
    {
        return $this->sexeClassement;
    }

    /**
     * Add sexeStatut
     *
     * @param \AppBundle\Entity\Statut $sexeStatut
     *
     * @return Sexe
     */
    public function addSexeStatut(\AppBundle\Entity\Statut $sexeStatut)
    {
        $this->sexeStatut[] = $sexeStatut;
    
        return $this;
    }

    /**
     * Remove sexeStatut
     *
     * @param \AppBundle\Entity\Statut $sexeStatut
     */
    public function removeSexeStatut(\AppBundle\Entity\Statut $sexeStatut)
    {
        $this->sexeStatut->removeElement($sexeStatut);
    }

    /**
     * Get sexeStatut
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSexeStatut()
    {
        return $this->sexeStatut;
    }
}
