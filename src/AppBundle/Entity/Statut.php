<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statut
 *
 * @ORM\Table(name="statut")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatutRepository")
 */
class Statut
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Statut_code", type="string", length=255)
     */
    private $statutCode;

    /**
     * @var string
     *
     * @ORM\Column(name="Statut_nom", type="string", length=255)
     */
    private $statutNom;

    /**
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinStatut")
     */
    private $statutBovin;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AnimalType", inversedBy="animalTypeStatut")
    * @ORM\JoinColumn(name="Statut_type", referencedColumnName="id")
    * 
    */
    private $statutType;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sexe", inversedBy="sexeStatut")
    * @ORM\JoinColumn(name="Statut_sexe", referencedColumnName="id")
    * 
    */
    private $statutSexe;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statutCode
     *
     * @param string $statutCode
     *
     * @return Statut
     */
    public function setStatutCode($statutCode)
    {
        $this->statutCode = $statutCode;

        return $this;
    }

    /**
     * Get statutCode
     *
     * @return string
     */
    public function getStatutCode()
    {
        return $this->statutCode;
    }

    /**
     * Set statutNom
     *
     * @param string $statutNom
     *
     * @return Statut
     */
    public function setStatutNom($statutNom)
    {
        $this->statutNom = $statutNom;

        return $this;
    }

    /**
     * Get statutNom
     *
     * @return string
     */
    public function getStatutNom()
    {
        return $this->statutNom;
    }

    /**
     * Get statutBovin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatutBovin()
    {
        return $this->statutBovin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->statutBovin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add statutBovin
     *
     * @param \BovinBundle\Entity\Bovin $statutBovin
     *
     * @return Statut
     */
    public function addStatutBovin(\BovinBundle\Entity\Bovin $statutBovin)
    {
        $this->statutBovin[] = $statutBovin;
    
        return $this;
    }

    /**
     * Remove statutBovin
     *
     * @param \BovinBundle\Entity\Bovin $statutBovin
     */
    public function removeStatutBovin(\BovinBundle\Entity\Bovin $statutBovin)
    {
        $this->statutBovin->removeElement($statutBovin);
    }

    /**
     * Set statutType
     *
     * @param \AppBundle\Entity\AnimalType $statutType
     *
     * @return Statut
     */
    public function setStatutType(\AppBundle\Entity\AnimalType $statutType = null)
    {
        $this->statutType = $statutType;
    
        return $this;
    }

    /**
     * Get statutType
     *
     * @return \AppBundle\Entity\AnimalType
     */
    public function getStatutType()
    {
        return $this->statutType;
    }

    /**
     * Set statutSexe
     *
     * @param \AppBundle\Entity\Sexe $statutSexe
     *
     * @return Statut
     */
    public function setStatutSexe(\AppBundle\Entity\Sexe $statutSexe = null)
    {
        $this->statutSexe = $statutSexe;
    
        return $this;
    }

    /**
     * Get statutSexe
     *
     * @return \AppBundle\Entity\Sexe
     */
    public function getStatutSexe()
    {
        return $this->statutSexe;
    }
}
