<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatutSortie
 *
 * @ORM\Table(name="statut_sortie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatutSortieRepository")
 */
class StatutSortie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="StatutSortie_code", type="string", length=255)
     */
    private $statutSortieCode;

    /**
     * @var string
     *
     * @ORM\Column(name="StatutSortie_nom", type="string", length=255)
     */
    private $statutSortieNom;

    /**
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinStatutSortie")
     */
    private $statutSortieBovin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statutSortieCode
     *
     * @param string $statutSortieCode
     *
     * @return StatutSortie
     */
    public function setStatutSortieCode($statutSortieCode)
    {
        $this->statutSortieCode = $statutSortieCode;

        return $this;
    }

    /**
     * Get statutSortieCode
     *
     * @return string
     */
    public function getStatutSortieCode()
    {
        return $this->statutSortieCode;
    }

    /**
     * Set statutSortieNom
     *
     * @param string $statutSortieNom
     *
     * @return StatutSortie
     */
    public function setStatutSortieNom($statutSortieNom)
    {
        $this->statutSortieNom = $statutSortieNom;

        return $this;
    }

    /**
     * Get statutSortieNom
     *
     * @return string
     */
    public function getStatutSortieNom()
    {
        return $this->statutSortieNom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->statutSortieBovin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add statutSortieBovin
     *
     * @param \BovinBundle\Entity\Bovin $statutSortieBovin
     *
     * @return StatutSortie
     */
    public function addStatutSortieBovin(\BovinBundle\Entity\Bovin $statutSortieBovin)
    {
        $this->statutSortieBovin[] = $statutSortieBovin;

        return $this;
    }

    /**
     * Remove statutSortieBovin
     *
     * @param \BovinBundle\Entity\Bovin $statutSortieBovin
     */
    public function removeStatutSortieBovin(\BovinBundle\Entity\Bovin $statutSortieBovin)
    {
        $this->statutSortieBovin->removeElement($statutSortieBovin);
    }

    /**
     * Get statutSortieBovin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatutSortieBovin()
    {
        return $this->statutSortieBovin;
    }
}
