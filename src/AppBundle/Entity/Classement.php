<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classement
 *
 * @ORM\Table(name="classement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClassementRepository")
 */
class Classement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Classement_nom", type="string", length=255)
     */
    private $classementNom;

    /**
     * @var string
     *
     * @ORM\Column(name="Classement_code", type="string", length=255)
     */
    private $classementCode;

    /**
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinClassement")
     */
    private $classementBovin;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AnimalType", inversedBy="animalTypeClassement")
    * @ORM\JoinColumn(name="Classement_type", referencedColumnName="id")
    * 
    */
    private $classementType;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sexe", inversedBy="sexeClassement")
    * @ORM\JoinColumn(name="Classement_sexe", referencedColumnName="id")
    * 
    */
    private $classementSexe;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set classementNom
     *
     * @param string $classementNom
     *
     * @return Classement
     */
    public function setClassementNom($classementNom)
    {
        $this->classementNom = $classementNom;

        return $this;
    }

    /**
     * Get classementNom
     *
     * @return string
     */
    public function getClassementNom()
    {
        return $this->classementNom;
    }

    /**
     * Set classementCode
     *
     * @param string $classementCode
     *
     * @return Classement
     */
    public function setClassementCode($classementCode)
    {
        $this->classementCode = $classementCode;

        return $this;
    }

    /**
     * Get classementCode
     *
     * @return string
     */
    public function getClassementCode()
    {
        return $this->classementCode;
    }


    /**
     * Get classementBovin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClassementBovin()
    {
        return $this->classementBovin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->classementBovin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add classementBovin
     *
     * @param \BovinBundle\Entity\Bovin $classementBovin
     *
     * @return Classement
     */
    public function addClassementBovin(\BovinBundle\Entity\Bovin $classementBovin)
    {
        $this->classementBovin[] = $classementBovin;
    
        return $this;
    }

    /**
     * Remove classementBovin
     *
     * @param \BovinBundle\Entity\Bovin $classementBovin
     */
    public function removeClassementBovin(\BovinBundle\Entity\Bovin $classementBovin)
    {
        $this->classementBovin->removeElement($classementBovin);
    }

    /**
     * Set classementType
     *
     * @param \AppBundle\Entity\AnimalType $classementType
     *
     * @return Classement
     */
    public function setClassementType(\AppBundle\Entity\AnimalType $classementType = null)
    {
        $this->classementType = $classementType;
    
        return $this;
    }

    /**
     * Get classementType
     *
     * @return \AppBundle\Entity\AnimalType
     */
    public function getClassementType()
    {
        return $this->classementType;
    }

    

    /**
     * Set classementSexe
     *
     * @param \AppBundle\Entity\Sexe $classementSexe
     *
     * @return Classement
     */
    public function setClassementSexe(\AppBundle\Entity\Sexe $classementSexe = null)
    {
        $this->classementSexe = $classementSexe;
    
        return $this;
    }

    /**
     * Get classementSexe
     *
     * @return \AppBundle\Entity\Sexe
     */
    public function getClassementSexe()
    {
        return $this->classementSexe;
    }
}
