<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnimalType
 *
 * @ORM\Table(name="animal_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnimalTypeRepository")
 */
class AnimalType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="AnimalType_nom", type="string", length=255)
     */
    private $animalTypeNom;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Race", mappedBy="raceType")
     */
    private $animalTypeRace;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Classement", mappedBy="classementType")
     */
    private $animalTypeClassement;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Statut", mappedBy="statutType")
     */
    private $animalTypeStatut;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Groupe", mappedBy="groupeAnimalTypes")
     * 
     */
    protected $animalTypeGroupes;

    /**
     * Constructor
     */
    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set animalTypeNom
     *
     * @param string $animalTypeNom
     *
     * @return AnimalType
     */
    public function setAnimalTypeNom($animalTypeNom)
    {
        $this->animalTypeNom = $animalTypeNom;
    
        return $this;
    }

    /**
     * Get animalTypeNom
     *
     * @return string
     */
    public function getAnimalTypeNom()
    {
        return $this->animalTypeNom;
    }

    /**
     * Add animalTypeRace
     *
     * @param \AppBundle\Entity\Race $animalTypeRace
     *
     * @return AnimalType
     */
    public function addAnimalTypeRace(\AppBundle\Entity\Race $animalTypeRace)
    {
        $this->animalTypeRace[] = $animalTypeRace;
    
        return $this;
    }

    /**
     * Remove animalTypeRace
     *
     * @param \AppBundle\Entity\Race $animalTypeRace
     */
    public function removeAnimalTypeRace(\AppBundle\Entity\Race $animalTypeRace)
    {
        $this->animalTypeRace->removeElement($animalTypeRace);
    }

    /**
     * Get animalTypeRace
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnimalTypeRace()
    {
        return $this->animalTypeRace;
    }

    /**
     * Add animalTypeClassement
     *
     * @param \AppBundle\Entity\Classement $animalTypeClassement
     *
     * @return AnimalType
     */
    public function addAnimalTypeClassement(\AppBundle\Entity\Classement $animalTypeClassement)
    {
        $this->animalTypeClassement[] = $animalTypeClassement;
    
        return $this;
    }

    /**
     * Remove animalTypeClassement
     *
     * @param \AppBundle\Entity\Classement $animalTypeClassement
     */
    public function removeAnimalTypeClassement(\AppBundle\Entity\Classement $animalTypeClassement)
    {
        $this->animalTypeClassement->removeElement($animalTypeClassement);
    }

    /**
     * Get animalTypeClassement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnimalTypeClassement()
    {
        return $this->animalTypeClassement;
    }

    /**
     * Add animalTypeStatut
     *
     * @param \AppBundle\Entity\Statut $animalTypeStatut
     *
     * @return AnimalType
     */
    public function addAnimalTypeStatut(\AppBundle\Entity\Statut $animalTypeStatut)
    {
        $this->animalTypeStatut[] = $animalTypeStatut;
    
        return $this;
    }

    /**
     * Remove animalTypeStatut
     *
     * @param \AppBundle\Entity\Statut $animalTypeStatut
     */
    public function removeAnimalTypeStatut(\AppBundle\Entity\Statut $animalTypeStatut)
    {
        $this->animalTypeStatut->removeElement($animalTypeStatut);
    }

    /**
     * Get animalTypeStatut
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnimalTypeStatut()
    {
        return $this->animalTypeStatut;
    }

    /**
     * Get animalTypeGroupes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnimalTypeGroupes()
    {
        return $this->animalTypeGroupes;
    }

    /**
     * Add animalTypeGroupe
     *
     * @param \UserBundle\Entity\Groupe $animalTypeGroupe
     *
     * @return AnimalType
     */
    public function addAnimalTypeGroupe(\UserBundle\Entity\Groupe $animalTypeGroupe)
    {
        $this->animalTypeGroupes[] = $animalTypeGroupe;

        return $this;
    }

    /**
     * Remove animalTypeGroupe
     *
     * @param \UserBundle\Entity\Groupe $animalTypeGroupe
     */
    public function removeAnimalTypeGroupe(\UserBundle\Entity\Groupe $animalTypeGroupe)
    {
        $this->animalTypeGroupes->removeElement($animalTypeGroupe);
    }
}
