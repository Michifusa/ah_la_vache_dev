<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NaissanceFacilite
 *
 * @ORM\Table(name="naissance_facilite")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NaissanceFaciliteRepository")
 */
class NaissanceFacilite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NaissanceFacilite_nom", type="string", length=255)
     */
    private $naissanceFaciliteNom;

    /**
     * @var string
     *
     * @ORM\Column(name="NaissanceFacilite_code", type="string", length=255)
     */
    private $naissanceFaciliteCode;

    /**
     *
     * @ORM\OneToMany(targetEntity="BovinBundle\Entity\Bovin", mappedBy="bovinNaissanceFacilite")
     *
     */
    private $naissanceFaciliteBovin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set naissanceFaciliteNom
     *
     * @param string $naissanceFaciliteNom
     *
     * @return NaissanceFacilite
     */
    public function setNaissanceFaciliteNom($naissanceFaciliteNom)
    {
        $this->naissanceFaciliteNom = $naissanceFaciliteNom;

        return $this;
    }

    /**
     * Get naissanceFaciliteNom
     *
     * @return string
     */
    public function getNaissanceFaciliteNom()
    {
        return $this->naissanceFaciliteNom;
    }

    /**
     * Set naissanceFaciliteCode
     *
     * @param string $naissanceFaciliteCode
     *
     * @return NaissanceFacilite
     */
    public function setNaissanceFaciliteCode($naissanceFaciliteCode)
    {
        $this->naissanceFaciliteCode = $naissanceFaciliteCode;

        return $this;
    }

    /**
     * Get naissanceFaciliteCode
     *
     * @return string
     */
    public function getNaissanceFaciliteCode()
    {
        return $this->naissanceFaciliteCode;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->naissanceFaciliteBovin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add naissanceFaciliteBovin
     *
     * @param \BovinBundle\Entity\Bovin $naissanceFaciliteBovin
     *
     * @return NaissanceFacilite
     */
    public function addNaissanceFaciliteBovin(\BovinBundle\Entity\Bovin $naissanceFaciliteBovin)
    {
        $this->naissanceFaciliteBovin[] = $naissanceFaciliteBovin;

        return $this;
    }

    /**
     * Remove naissanceFaciliteBovin
     *
     * @param \BovinBundle\Entity\Bovin $naissanceFaciliteBovin
     */
    public function removeNaissanceFaciliteBovin(\BovinBundle\Entity\Bovin $naissanceFaciliteBovin)
    {
        $this->naissanceFaciliteBovin->removeElement($naissanceFaciliteBovin);
    }

    /**
     * Get naissanceFaciliteBovin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNaissanceFaciliteBovin()
    {
        return $this->naissanceFaciliteBovin;
    }
}
