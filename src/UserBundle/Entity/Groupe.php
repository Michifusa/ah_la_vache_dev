<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * Groupe
 *
 * @ORM\Table(name="groupe")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\GroupeRepository")
 */
class Groupe extends BaseGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Groupe_adresse", type="string", length=255)
     */
    private $groupeAdresse;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\AnimalType")
     * @ORM\JoinTable(name="animaltype_group",
     *      joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="animaltype_id", referencedColumnName="id")}
     * )
     */
    protected $groupeAnimalTypes;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", mappedBy="groups")
     * 
     */
    protected $users;

    /**
    * @var INT
    *
    * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="userChefGroupe")
    * @ORM\JoinColumn(name="Groupe_userChef", referencedColumnName="id")
    * 
    */
    private $groupeUserChef;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupeUser = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupeAnimal = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set groupeAdresse
     *
     * @param string $groupeAdresse
     *
     * @return Groupe
     */
    public function setGroupeAdresse($groupeAdresse)
    {
        $this->groupeAdresse = $groupeAdresse;

        return $this;
    }

    /**
     * Get groupeAdresse
     *
     * @return string
     */
    public function getGroupeAdresse()
    {
        return $this->groupeAdresse;
    }

    /**
     * Add groupeUser
     *
     * @param \UserBundle\Entity\User $groupeUser
     *
     * @return Groupe
     */
    public function addGroupeUser(\UserBundle\Entity\User $groupeUser)
    {
        $this->groupeUser[] = $groupeUser;

        return $this;
    }

    /**
     * Remove groupeUser
     *
     * @param \UserBundle\Entity\User $groupeUser
     */
    public function removeGroupeUser(\UserBundle\Entity\User $groupeUser)
    {
        $this->groupeUser->removeElement($groupeUser);
    }

    /**
     * Get groupeUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupeUser()
    {
        return $this->groupeUser;
    }

    /**
     * Add user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Groupe
     */
    public function addUser(\UserBundle\Entity\User $user)
    {
        if (!$this->getUsers()->contains($user)) {
            $this->getUsers()->add($user);
            $user->addGroup($this);
        }

        return $this;
    }

    /**
     * Remove user
     *
     * @param \UserBundle\Entity\User $user
     */
    public function removeUser(\UserBundle\Entity\User $user)
    {
        if ($this->getUsers()->contains($user)) {
            $this->getUsers()->removeElement($user);
            $user->removeGroup($this);
        }

        return $this;
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Get groupeAnimalTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupeAnimalTypes()
    {
        return $this->groupeAnimalTypes;
    }

    /**
     * Add groupeAnimalType
     *
     * @param \AppBundle\Entity\AnimalType $groupeAnimalType
     *
     * @return Groupe
     */
    public function addGroupeAnimalType(\AppBundle\Entity\AnimalType $groupeAnimalType)
    {
        $this->groupeAnimalTypes[] = $groupeAnimalType;

        return $this;
    }

    /**
     * Remove groupeAnimalType
     *
     * @param \AppBundle\Entity\AnimalType $groupeAnimalType
     */
    public function removeGroupeAnimalType(\AppBundle\Entity\AnimalType $groupeAnimalType)
    {
        $this->groupeAnimalTypes->removeElement($groupeAnimalType);
    }

    /**
     * Set groupeUserChef
     *
     * @param \UserBundle\Entity\User $groupeUserChef
     *
     * @return Groupe
     */
    public function setGroupeUserChef(\UserBundle\Entity\User $groupeUserChef = null)
    {
        $this->groupeUserChef = $groupeUserChef;

        return $this;
    }

    /**
     * Get groupeUserChef
     *
     * @return \UserBundle\Entity\User
     */
    public function getGroupeUserChef()
    {
        return $this->groupeUserChef;
    }
}
