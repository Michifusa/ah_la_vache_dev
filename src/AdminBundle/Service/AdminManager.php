<?php
namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\Groupe;

class AdminManager{

	private $entityManager;

    public function __construct(EntityManager $entityManager,ContainerInterface $containerInterface)
    {
    	$this->em = $entityManager;
    	$this->repositoryBovin = $this->em->getRepository('BovinBundle:Bovin');
    	$this->repositorySexe = $this->em->getRepository('AppBundle:Sexe');
    	$this->repositoryGroupe = $this->em->getRepository('UserBundle:Groupe');
        $this->repositoryUser = $this->em->getRepository('UserBundle:User');
        $this->repositoryAnimalType = $this->em->getRepository('AppBundle:AnimalType');
    	$this->container = $containerInterface;
        $this->root = $this->container->get('kernel')->getRootDir();
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();
    }

    public function SearchAllAnimalTypes()
    {
        $animalTypes = $this->repositoryAnimalType ->findAll();
        return $animalTypes;
    }
    public function SearchAllGroups()
    {
        $groupes = $this->repositoryGroupe->findAll();

        return $groupes;
    }

    public function SearchGroupById($groupe_id)
    {
        $groupe = $this->repositoryGroupe->find($groupe_id);

        return $groupe;
    }

    public function SearchAllUsers()
    {
        $users = $this->repositoryUser->findAll();

        return $users;
    }

    public function SearchUserById($user_id)
    {
        $user = $this->repositoryUser->find($user_id);

        return $user;
    }

    public function AjoutModifGroup($request){

        if($request->query->get('type') == 'addGroupe'){
            $groupe = new Groupe();
        }else{
            $groupe = $this->repositoryGroupe->find($request->query->get('id'));
        }

        switch ($request->query->get('type')) {

            case 'addGroupe':
                $groupe->setGroupeAdresse($request->request->get('groupeAdresse'));
                $groupe->setName($request->request->get('groupeNom'));
                $userChef = $this->repositoryUser->find($request->request->get('groupeChef'));
                $groupe->setGroupeUserChef($userChef);
                break;
            
            case 'modifAdresseAndNom':
                if($request->request->get('groupeAdresse')){
                    $groupe->setGroupeAdresse($request->request->get('groupeAdresse'));
                }

                if($request->request->get('groupeNom')){
                    $groupe->setName($request->request->get('groupeNom'));
                }
                break;

            case 'changeGroupeChef':
                $user = $this->repositoryUser->find($request->query->get('groupeChef_id'));
                $groupe->setGroupeUserChef($user);
                break;

            case 'addRemoveMembre':
                $action = $request->query->get('action');
                $user = $this->repositoryUser->find($request->query->get('user_id'));

                if($action == 'add'){
                    $groupe->addUser($user);
                }else if($action == 'remove'){
                    $groupe->removeUser($user);
                }
                break;

            case 'addRemoveCategorie':
                $action = $request->query->get('action');
                $animaltype = $this->repositoryAnimalType->find($request->query->get('animaltype_id'));

                if($action == 'add'){
                    $groupe->addGroupeAnimalType($animaltype);
                }else if($action == 'remove'){
                    $groupe->removeGroupeAnimalType($animaltype);
                }
                break;

            case 7:
                
                break;    
        }

        $this->em->persist($groupe);
        $this->em->flush();

        if($request->query->get('type') == 'addGroupe'){
            mkdir($this->container->getParameter('kernel.root_dir').'/../web/upload/groupe/'.$groupe->getId(), 0700);
        }
                
        
    }

    public function RemoveGroup($id_group)
    {
        $groupe = $this->repositoryGroupe->find($id_group);

        $this->em->remove($groupe);
        $this->em->flush();

    }
}