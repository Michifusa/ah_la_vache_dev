<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class AdminController extends Controller
{
    /**
     * @Route("/", name="admin")
     * @Method({"GET"})
     */
    public function AdminPageAction(Request $request)
    {
        return $this->render('AdminBundle:Admin:index.html.twig');
    }

    /**
     * @Route("/groupes/", name="admin_groups")
     * @Method({"GET"})
     */
    public function AdminGroupsPageAction(Request $request)
    {
    	$adminmanager = $this->container->get('adminbundle.adminmanager');
        $users = $adminmanager->SearchAllUsers();

        return $this->render('AdminBundle:Group:admin_groups.html.twig',['users'=>$users]);
    }

    /**
     * @Route("/groupes/infos/{id}", name="admin_groups_infos", requirements={"id"="\d+"})
     * @Method({"GET"})
     */
    public function AdminGroupsInfosPageAction(Request $request,$id)
    {
        $adminmanager = $this->container->get('adminbundle.adminmanager');
        $groupe = $adminmanager->SearchGroupById($id);
        $users = $adminmanager->SearchAllUsers();
        $animalTypes = $adminmanager->SearchAllAnimalTypes();

        if($groupe){
            return $this->render('AdminBundle:Group:admin_groups_infos.html.twig',['groupe'=>$groupe,'users'=>$users,'animalTypes'=>$animalTypes]);
        }else{
            return new RedirectResponse($this->generateUrl('admin_groups'));
        }
        
    }

    /**
     * @Route("/groupes/load-table", name="admin_load_table_group", options={"expose"=true})
     * @Method({"GET"})
     */
    public function AdminGroupsTableLoadAction(Request $request)
    {
        $adminmanager = $this->container->get('adminbundle.adminmanager');
        $groupes = $adminmanager->SearchAllGroups();

        return $this->render('AdminBundle:Group/Table:table_list_group.html.twig',['groups_list'=>$groupes]);
    }


    /**
     * @Route("/utilisateurs/", name="admin_users")
     * @Method({"GET"})
     */
    public function AdminUsersPageAction(Request $request)
    {
    	$adminmanager = $this->container->get('adminbundle.adminmanager');

        return $this->render('AdminBundle:User:admin_users.html.twig');
    }

    /**
     * @Route("/utilisateurs/infos/{id}", name="admin_users_infos", requirements={"id"="\d+"})
     * @Method({"GET"})
     */
    public function AdminUsersInfosPageAction(Request $request,$id)
    {
        $adminmanager = $this->container->get('adminbundle.adminmanager');
        $user = $adminmanager->SearchUserById($id);

        if($user){
            return $this->render('AdminBundle:User:admin_users_infos.html.twig',['user'=>$user]);
        }else{
            return new RedirectResponse($this->generateUrl('admin_users'));
        }
    }

    /**
     * @Route("/utilisateurs/load-table", name="admin_load_table_user", options={"expose"=true})
     * @Method({"GET"})
     */
    public function AdminUsersTableLoadAction(Request $request)
    {
        $adminmanager = $this->container->get('adminbundle.adminmanager');
        $users = $adminmanager->SearchAllUsers();

        return $this->render('AdminBundle:User/Table:table_list_user.html.twig',['users_list'=>$users]);
    }

    /**
     * @Route("/groupes/modif-add", name="admin_groups_modif_add", options={"expose"=true})
     * @Method({"POST"})
     */
    public function AdminGroupsModifAddAction(Request $request)
    {
        $adminmanager = $this->container->get('adminbundle.adminmanager');
        $id_groupe = $adminmanager->AjoutModifGroup($request);
        
        return new Response('ok');
    }

    /**
     * @Route("/groupes/remove/{id}", requirements={"id"="\d+"}, name="admin_groups_remove", options={"expose"=true})
     * @Method({"POST"})
     */
    public function AdminGroupsRemoveAction(Request $request,$id)
    {
        $adminmanager = $this->container->get('adminbundle.adminmanager');
        $adminmanager->RemoveGroup($id);
        
        return new Response('ok');
    }
}
